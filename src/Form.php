<?php

namespace GeneratoreFatturaElettronica;

/**
 * Class Form
 * used to generate html forms
 * @package GeneratoreFatturaElettronica
 */
class Form
{
    private $fields = [];

    public function __construct($action = '')
    {
        $this->fields = [];
    }

    public function setAction($action)
    {
        $this->action = $action;
    }

    public function getAction()
    {
        return $this->action ?? "";
    }

    public function setMethod($method)
    {
        $this->method = $method;
    }

    public function getMethod()
    {
        return $this->method ?? "GET";
    }

    public function setID($id)
    {
        $this->id = $id;
    }

    public function getID()
    {
        return $this->id ?? "";
    }

    public function addHeader($text)
    {
        $this->fields[] = '<h3>' . $text . '</h3>';
    }

    public function addSeparator()
    {
        $this->fields[] = '<hr>';
    }

    public function addHtml($html)
    {
        $this->fields[] = $html;
    }

    public function addFormGroup($label, $input, $help = "", $class = "col-4", $id = "")
    {
        $this->fields[] = $this->formGroup($label, $input, $help, $class, $id);
    }

    public function formGroup($label, $input, $help = "", $class = "col-4", $id = "")
    {
        if ($label != "") {
            $label = '<label class="' . $class . ' col-form-label">' . $label . '</label>';
        }
        return '<div class="form-group row align-items-center" id="' . $id . '">
                    ' . $label . '
                    <div class="col">
                        ' . $input . '
                        <small class="text-muted">' . $help . '</small>
                    </div>
                </div>';
    }

    public function addField($name, $label = null, $settings = [])
    {
        $label = $label ?? ucfirst($name);
        $settings = array_merge(['type' => 'text'], $settings);
        $placeholder = $settings['placeholder'] ?? $label;
        $value = $settings['value'] ?? "";
        $help = $settings['help'] ?? "";
        if ($settings['type'] == 'select') {
            $input = '<select class="form-control" name="' . $name . '">';
            foreach ($settings['options'] as $val => $option) {
                $input .= '<option value="' . $val . '">' . $option . '</option>';
            }
            $input .= '</select>';
        } else {
            $input = '<input type="' . $settings['type'] . '" name="' . $name . '" value="' . $value . '" class="form-control" placeholder="' . $placeholder . '">';
        }
        $this->fields[] = $this->formGroup($label, $input, $help);

    }

    public function display()
    {
        ?>
        <form action="<?php echo $this->getAction() ?>" method="<?php echo $this->getMethod() ?>"
              id="<?php echo $this->getID() ?>">
            <?php
            foreach ($this->fields as $field) {
                echo $field;
            }
            ?>
            <hr>
            <input type="submit" value="Conferma" class="btn btn-success btn-lg d-block mx-auto">
        </form>
        <?php
    }


}