<?php

namespace GeneratoreFatturaElettronica;


class Request
{
    public static $instance = null;

    public function __construct()
    {
    }


    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function get($key, $default = null)
    {
        return $_REQUEST[$key] ?? $default;
    }

}