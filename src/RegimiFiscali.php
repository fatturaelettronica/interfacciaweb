<?php

namespace GeneratoreFatturaElettronica;


use Deved\FatturaElettronica\Codifiche\RegimeFiscale;

class RegimiFiscali extends RegimeFiscale
{

    use OttenitoreCodifiche;

}