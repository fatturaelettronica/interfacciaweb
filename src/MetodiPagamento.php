<?php

namespace GeneratoreFatturaElettronica;


use Deved\FatturaElettronica\Codifiche\ModalitaPagamento;

class MetodiPagamento extends ModalitaPagamento
{

    use OttenitoreCodifiche;
}