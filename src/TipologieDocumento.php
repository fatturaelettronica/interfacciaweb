<?php

namespace GeneratoreFatturaElettronica;


use Deved\FatturaElettronica\Codifiche\TipoDocumento;

class TipologieDocumento extends TipoDocumento
{
    use OttenitoreCodifiche;

}