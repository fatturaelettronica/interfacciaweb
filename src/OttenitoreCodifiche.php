<?php

namespace GeneratoreFatturaElettronica;


trait OttenitoreCodifiche
{
    public static function all()
    {
        $codifiche = self::$codifiche;
        foreach ($codifiche as $key => $codifica) {
            $codifiche[$key] = $codifica . " ($key)";
        }
        return $codifiche;
    }
}