<?php

use GeneratoreFatturaElettronica\{RegimiFiscali, Form, MetodiPagamento, TipologieDocumento};

$form = new Form();
$form->setMethod('POST');
$form->addHeader('Dati anagrafici Cedente');

$form->addField('ragionesociale', 'Ragione Sociale');
$form->addField('cf', 'Codice Fiscale');
$form->addField('codice_paese', 'Paese', ['value' => 'IT', 'help' => 'Codice ISO del paese']);
$form->addField('sdi_code', 'Codice SDI', ['help' => 'Codice per Sistema Di Interscambio']);
$form->addField('regime', 'Regime fiscale', [
    'type' => 'select',
    'options' => array_merge(['' => 'Seleziona un regime fiscale'], RegimiFiscali::all())
]);

$form->addSeparator();

$form->addField('telefono');
$form->addField('email', null, ['type' => 'email']);

$form->addSeparator();

$form->addField('nazione');
$form->addField('indirizzo');
$form->addField('cap', 'CAP', ['type' => 'number']);
$form->addField('comune');
$form->addField('provincia');

$form->addHeader('Dati anagrafici Cessionario');

$form->addField('cessionario_ragionesociale', 'Ragione Sociale');
$form->addField('cessionario_cf', 'Codice Fiscale');
$form->addField('cessionario_nazione', 'Nazione');
$form->addField('cessionario_indirizzo', 'Indirizzo');
$form->addField('cessionario_cap', 'CAP', ['type' => 'number']);
$form->addField('cessionario_comune', 'Comune');
$form->addField('cessionario_provincia', 'Provincia');


$form->addHeader('Informazioni Documento');

$form->addField('tipo_documento', 'Tipo Documento', [
    'type' => 'select',
    'options' => array_merge(['' => 'Seleziona una tipologia di documento'], TipologieDocumento::all())
]);
$form->addField('data', 'Data della fattura', ['type' => 'date']);
$form->addField('numero', 'Numero fattura');
$form->addField('importo_totale', 'Totale Documento', ['type' => 'number']);

$form->addHeader('Informazioni Pagamento');

$form->addField('metodo_pagamento', 'Metodo di pagamento', [
    'type' => 'select',
    'options' => array_merge(['' => 'Seleziona un metodo di pagamento'], MetodiPagamento::all())
]);
$form->addField('data_scadenza_pagamento', 'Data scadenza pagamento', ['type' => 'date']);
$form->addField('importo_pagamento', 'Totale Pagamento', ['type' => 'number']);

$form->addHeader('Articoli/Prodotti/Servizi');
$form->addFormGroup(
    '', '
        <div class="row articolo">
            <div class="col">
                <input type="text" name="articoli[0][descrizione]" class="form-control" placeholder="Descrizione">
            </div>
            <div class="col">
                <input type="number" name="articoli[0][prezzo]" class="form-control" placeholder="Prezzo Unitario">
            </div>
            <div class="col">
                <input type="text" name="articoli[0][codice]" class="form-control" placeholder="Codice Articolo">
            </div>
            <div class="col">
                <button class="btn btn-outline-secondary articolo-adder w-100">+</button>
            </div>
        </div>
    ', '', '', 'articoli'
);

$form->addHeader('Informazioni Aggiuntive XML');

$form->addField('progresso_invio', 'Progresso invio', ['value' => '001', 'help' => 'Inserire il numero dell\'ultima fattura inviata <strong>+1</strong>']);


$form->display();