<?php
require __DIR__ . '/vendor/autoload.php';

use Deved\FatturaElettronica\FatturaElettronica\FatturaElettronicaBody\DatiBeniServizi\{DettaglioLinee, Linea};
use Deved\FatturaElettronica\FatturaElettronica\FatturaElettronicaHeader\Common\{DatiAnagrafici, Sede};
use Deved\FatturaElettronica\FatturaElettronicaFactory;
use Deved\FatturaElettronica\FatturaElettronica\FatturaElettronicaBody\{DatiGenerali, DatiPagamento};
use SlamFatturaElettronica\Validator;
use GeneratoreFatturaElettronica\Request;

$request = Request::getInstance();
$xml = '';

if ($request->get('progresso_invio', false)) {
    $anagraficaCedente = new DatiAnagrafici(
        $request->get('cf'),
        $request->get('ragionesociale'),
        $request->get('codice_paese'),
        $request->get('sdi_code'),
        $request->get('regime')
    );
    $sedeCedente = new Sede(
        $request->get('nazione'),
        $request->get('indirizzo'),
        $request->get('cap'),
        $request->get('comune'),
        $request->get('provincia')
    );
    $fatturaElettronicaFactory = new FatturaElettronicaFactory(
        $anagraficaCedente,
        $sedeCedente,
        $request->get('telefono'),
        $request->get('email')
    );
    $anagraficaCessionario = new DatiAnagrafici(
        $request->get('cessionario_cf'),
        $request->get('cessionario_ragionesociale')
    );
    $sedeCessionario = new Sede(
        $request->get('cessionario_nazione'),
        $request->get('cessionario_indirizzo'),
        $request->get('cessionario_cap'),
        $request->get('cessionario_comune'),
        $request->get('cessionario_provincia')
    );
    $fatturaElettronicaFactory->setCessionarioCommittente($anagraficaCessionario, $sedeCessionario);

    $datiGenerali = new DatiGenerali(
        $request->get('tipo_documento'),
        $request->get('data'),
        $request->get('numero'),
        $request->get('importo_totale')
    );

    $datiPagamento = new DatiPagamento(
        $request->get('metodo_pagamento'),
        $request->get('data_scadenza_pagamento'),
        $request->get('importo_pagamento')
    );
    $linee = [];
    foreach ($request->get('articoli') as $articolo) {
        if (isset($articolo['descrizione']) && isset($articolo['prezzo']) && $articolo['codice']) {
            $linee[] = new Linea($articolo['descrizione'], intval($articolo['prezzo']), $articolo['codice']);
        }
    }

    $dettaglioLinee = new DettaglioLinee($linee);

    $fattura = $fatturaElettronicaFactory->create(
        $datiGenerali,
        $datiPagamento,
        $dettaglioLinee,
        $request->get('progresso_invio')
    );
    $file = $fattura->getFileName();
    $xml = $fattura->toXml();
}
?>

<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.13.1/styles/arduino-light.min.css"
          integrity="sha256-dArcFF7NmN+jP/jXVYhMqvzzTN+bNNqBlu7TSD8F8iM=" crossorigin="anonymous"/>
    <title>Generatore Fattura XML </title>
</head>
<body>
<div class="container-fluid">
    <div class="row my-5">
        <div class="col text-center"><h1>Fattura elettronica</h1></div>
    </div>
    <div class="row">
        <div class="col">
            <?php
            if ($xml) {
                $validatoreFattura = new Validator();
                $valid = true;
                try {
                    $validatoreFattura->assertValidXml($xml);
                } catch (\SlamFatturaElettronica\Exception\InvalidXmlStructureException $invalidXmlStructureException) {
                    ?>
                    <div class="alert alert-warning">
                        <h3>Struttura XML non conforme</h3>
                        <?php echo $invalidXmlStructureException->getMessage(); ?>
                    </div>
                    <?php
                    $valid = false;

                } catch (\SlamFatturaElettronica\Exception\InvalidXsdStructureComplianceException $complianceException) {
                    ?>
                    <div class="alert alert-danger">
                        <h3>Dati non conformi</h3>
                        <?php echo $complianceException->getMessage(); ?>
                    </div>
                    <?php
                    $valid = false;
                }
                if ($valid) {
                    ?>
                    <div class="alert alert-success">
                        <h3>Fattura generata correttamente</h3>
                        <p>Il nome sugerito è il seguente <code><?php echo $file?></code></p>
                    </div>
                    <?php
                }
            }
            ?>
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <h2 class="text-center">DATI</h2>
            <?php require __DIR__ . '/form.php' ?>
        </div>
        <div class="col-md-6">
            <h2 class="text-center">XML
                <a href="#" class="btn  btn-sm btn-outline-primary" id="xml-button">Copia negli appunti</a></h2>
            <small id="copied-message" class="text-success" style="display: none;">XML copiato negli appunti</small>
            <pre id="xml"><code class="xml"><?php echo htmlspecialchars($xml) ?></code></pre>
        </div>
    </div>
</div>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
        integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/2.0.4/clipboard.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.13.1/highlight.min.js"></script>
<script>
    (function ($) {
        $('[data-toggle="tooltip"]').tooltip();
        hljs.initHighlightingOnLoad();
        xmlbuttonCopy = new ClipboardJS('#xml-button', {
            text: function (t) {
                return $('#xml').text()
            }
        });
        xmlbuttonCopy.on('success', function (e) {
            $('#copied-message').show(0);
            setTimeout(function () {
                $('#copied-message').hide(0);
            }, 2500);
            e.clearSelection();
        });
        var articolo = $('.articolo').clone();
        $(document).on('click', '.articolo-adder', function (e) {
            e.preventDefault();
            alert('funzione non disponibile');
            /*$('#articoli > .col').append(articolo.addClass('mt-3'));
            articolo = articolo.clone();*/
        });
    })(jQuery)
</script>
</body>
</html>
